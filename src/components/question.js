import React from 'react';
import Flags from './flags'

export default class Question extends React.Component {
    render() {
        const correct = this.props.correct;
        const incorrect = this.props.incorrect;
        var onSelect = function (code) {
            if (answer.code === code) {
                correct();
            } else {
                incorrect();
            }
        }
        const answer = this.props.answer;
        const options = this.props.options;
        return (
            <div>
                <h1>Which of these is the flag of {answer ? answer.name : ''}?</h1>
                <Flags onSelect={onSelect} titles={false} flags={options}></Flags>
            </div>
        )
    }
}
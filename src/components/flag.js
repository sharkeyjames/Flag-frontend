import React from 'react'

export default class Flag extends React.Component {
    render() {
        if (this.props.flag && this.props.onSelect) {
            var code = this.props.flag.code
            var click = () => {
                this.props.onSelect(code);
            }

            return (
                <div className="flag">
                    <div className="img-wrapper">
                        <img onClick={click} src={'/assets/data/svg/' + code.toLowerCase() + '.svg'} className="flag-img" alt="flag" />
                    </div>
                </div>
            )
        }
        return <h1>No Flag info</h1>;
    }
}
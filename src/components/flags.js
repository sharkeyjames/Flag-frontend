import React from 'react';
import Flag from './flag'

export default class Flags extends React.Component {
    render() {
        const flags = this.props.flags;
        const titles = this.props.titles;
        var click = (code) => {
            this.props.onSelect(code);
        }
        let flag = '';
        if (Array.isArray(flags)) {
            var y = []
            flag = flags.forEach(async obj => {
                var x = '';
                if (this.titles) {
                    x = (
                        <div class="flag-and-name">
                            <h1>{obj.name}</h1>
                            <Flag onSelect={click} key={obj.code} flag={obj}></Flag>
                        </div>
                    )
                } else {
                    x = (
                        <div className="flag-wrapper">
                            <Flag onSelect={click} key={obj.code} flag={obj}></Flag>
                        </div>
                    )
                }
                y.push(x);
            })

            var even = [];
            var odd = [];
            for (var i = 0; i < y.length; i += 2) {
                even.push(y[i]);
                y[i + 1] && odd.push(y[i + 1]);
            }
            return (
                <div className="row">

                    <div className="column">
                        {even}
                    </div>

                    <div className="column">
                        {odd}
                    </div>

                </div>
            )
        }
        return (
            <div className="row container">
                {flag}
                {titles}
            </div>
        )
    }
}

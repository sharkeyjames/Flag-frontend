import React from 'react';

export default class Score extends React.Component {
    render() {
        return (
            <div className="score">

                <div className="row">

                    <div className="column">

                        <div className="green">

                            <h2>{this.props.correct}</h2>

                        </div>

                    </div>

                    <div className="column">

                        <div className="red">

                            <h2>{this.props.incorrect}</h2>

                        </div>

                    </div>

                </div>

            </div>
        )
    }
}
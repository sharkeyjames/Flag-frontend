import React, { Component } from 'react';
import logo from './logo.jpg';
import './App.css';
import Question from './components/question'
import axios from '../node_modules/axios';
import Score from './components/score';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      answer: {},
      options: {},
      correct: 0,
      incorrect: 0
    };
  }


  componentDidMount() {
    axios.get('http://localhost:8080/countries/')
      .then(response => {
        this.setState({
          flags: response.data
        });

      })
      .catch(function (error) {
        console.log(error);
      });

    const insert = this.insert()

    axios.get('http://localhost:8080/question/')
      .then(response => {
        this.setState({
          answer: response.data.answer,
          options: insert(response.data.options, Math.floor(Math.random() * (response.data.options.length + 1)), response.data.answer) // (response.data.options).splice(Math.floor(Math.random() * response.data.options.length), 0, response.data.answer)
        });
      })
      .catch(function (error) {
        console.log(error);
      });

  }

  getQuestion(insert) {
    axios.get('http://localhost:8080/question/')
      .then(response => {
        this.setState({
          answer: response.data.answer,
          options: insert(response.data.options, Math.floor(Math.random() * (response.data.options.length + 1)), response.data.answer) // (response.data.options).splice(Math.floor(Math.random() * response.data.options.length), 0, response.data.answer)
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  insert() {
    return (arr, index, newItem) => [
      ...arr.slice(0, index),
      newItem,
      ...arr.slice(index)
    ];
  }

  render() {
    const insert = this.insert()
    var correct = () => {
      this.setState({
        correct: this.state.correct + 1
      });
      this.getQuestion(insert)
    }
    var incorrect = () => {
      this.setState({
        incorrect: this.state.incorrect + 1
      });
      this.getQuestion(insert)
    }
    const answer = this.state.answer;
    const options = this.state.options;
    return (
      <div className="App">
        <Score correct={this.state.correct} incorrect={this.state.incorrect}></Score>
        <header className="App-header">
          <Question correct={correct} incorrect={incorrect} answer={answer} options={options}></Question>
          <img src={logo} className="App-logo" alt="logo" />
        </header>
      </div>
    );
  }
}
